%% Montecarlo simulation 

clc
clear
close all

iter = 10000;                % Number of iterations
prices = 6000:3000:15000;    % Possible prices

%% Graphical User Interface (GUI)

list = {'Create a new cell','Use an existing cell (montecarlo.mat)','Read excel file (montecarlo.xlsm)'};
input = listdlg('PromptString',{'Data input.'},...
    'SelectionMode','single','ListSize',[300,100],'ListString',list);

%% Election from the GUI code to get montecarlo cell

%       input = 1 ; Manual input of the data
%       input = 2 ; Read previous montecarlo.mat file data
%       input = 3 ; Read montecarlo.xlsm file

if input == 1 % Manual imput of the data
    prompt = {'Number of elements'};
    dlgtitle = 'Data input ';
    dims = [1 40];
    definput = {'0'};
    n = inputdlg(prompt,dlgtitle,dims,definput);
    n = str2double(n);
    montecarlo = cell(n,5);
    
    if  n ~= 0
        uiwait(msgbox({'Enter first critical risks in order of priority.';'Possible types of risk:';...
            '  1. Critical risk (CRIT)';'  2. Normal risk (RISK)';'  3. Fixed price risk (FIX)'},...
            'Info','help'))   
        answer = [];
        for i = 1:n
            prompt = {'Name of the risk','Percentage of car price [%] / Fixed cost [€]','Risk probability [%]','Type of risk (RISK or CRIT)'};
            dlgtitle = (['Element ',num2str(i),' of',num2str(n)]);
            dims = [1 50; 1 50; 1 50; 1 50];
            definput = {'-','0','0','RISK'};
            answer = inputdlg(prompt,dlgtitle,dims,definput);
            if isempty(answer) == 1 
                return
            end
            montecarlo{i,1} = i;
            montecarlo{i,2} = answer{1,1};
            montecarlo{i,3} = str2double(answer{2,1});
            montecarlo{i,4} = str2double(answer{3,1});
            montecarlo{i,5} = answer{4,1};
        end
        save('montecarlo.mat','montecarlo')
    else
        return
    end
    
elseif input==2 % Read previous montecarlo.mat file data
    load montecarlo.mat
    [n,~] = size(montecarlo);
    
else % input ==3 ; Read montecarlo.xlsm file
    filename='montecarlo.xlsm';
    sheet=1;
    xlRange='A2:F11';
    [xa, xb] = xlsread(filename,sheet,xlRange);
    xb(:,3)=num2cell(xa(:,1));
    xb(:,4)=num2cell(xa(:,2));
    montecarlo=xb;
    
    n=length(montecarlo);
    save('montecarlo.mat','montecarlo') 
   
end

%% Montecarlo simulation code

col = [[0, 0.4470, 0.7410];       % Colors
      0.4660, 0.6740, 0.1880;
      0.9290, 0.6940, 0.1250;
      0.8500, 0.3250, 0.0980];
  
table = zeros(length(prices),7);  % Table with results
fig = zeros(length(prices),1); 
for m = 1:length(prices)
    values = zeros(n,2);
    c=0;
    for i = 1:n
        if strcmp(montecarlo{i,5},'FIX') == 0
            values(i,1) = montecarlo{i,3}*prices(m)/100;
            values(i,2) = montecarlo{i,4};
        else
            values(i,1) = montecarlo{i,3};
            values(i,2) = montecarlo{i,4};
        end
        if strcmp(montecarlo{i,5},'CRIT') == 1
            c = i;
        end
    end
    
    res1 = zeros(iter,n+1);
    res2 = zeros(iter,2);
    for i = 1:iter
    random = rand(n,1)*100;
    res1(i,1:n) = random(:,1) < values(:,2);
        for k = 1:c
            if res1(i,k) == 1
                res1(i,k+1:end-1) = 0;
            end
        end
    res1(i,n+1) = dot(res1(i,1:n), values(:,1)); 
    end

    res2(:,1) = linspace(100/iter,100,iter);
    b = sortrows(res1,n+1);
    res2(:,2) = b(:,n+1);

    % Table
    nomsavperc = 100*5000/prices(m);
    nomsav = 5000;
    sav75 = 5000-res2(round(75*iter/100),2);
    sav80 = 5000-res2(round(80*iter/100),2);
    sav85 = 5000-res2(round(85*iter/100),2);
    sav90 = 5000-res2(round(90*iter/100),2);
    table(m,:)=[prices(m) nomsavperc nomsav sav75 sav80 sav85 sav90];

    % Plot single graph
    fig(m+2) = figure(m+2);
    title(['Montecarlo overcosts beyond Cost Estimate (car price: ',num2str(prices(m)),'€)'])
    hold on
    plot(res2(:,1),res2(:,2),'-','Color', col(m,:),'LineWidth',2)
    plot([0 100],[5000 5000],'--','Color', col(m,:),'LineWidth',1)
    ylabel('Contingency','fontsize',12); ytickformat('eur'); ylim([0 10000]);
    xlabel('Confidence','fontsize',12); xtickformat('percentage')
    axis([0 100 min(res2(:,2)) max(res2(:,2))]);
    annotation('textbox', [0.25, 0.7, 0.1, 0.1],...
    'String', " - - Nominal saving (5000€)")
    set(fig(m+2),'Position',[20 80 800 400])
    
    % Plot overall graph
    fig(1) = figure(1);
    set(fig(1),'Position',[20 580 800 400])
    title('Montecarlo overcosts beyond Cost Estimate')
    hold on
    plot(linspace(0,100,iter),nomsavperc*ones(iter,1),'--','Color', col(m,:),'LineWidth',1); hold on;
    plot([0 100],[0 0],'k-','LineWidth',0.2);
    plot(res2(:,1),100*res2(:,2)/prices(m),'-','Color', col(m,:),'LineWidth',2); 
    ylabel('Contingency','fontsize',12); ytickformat('percentage'); 
    xlabel('Confidence','fontsize',12); xtickformat('percentage');
    set(gca,'fontsize',12,'box','on')
    axis([0 100 min(100*res2(:,2)/prices(m)) max(100*res2(:,2)/prices(m))]);
    hold on

end

% Plot overall graph
ylabel('Contingency (% of car price)','fontsize',12); ytickformat('percentage'); 
xlabel('Confidence','fontsize',12); xtickformat('percentage');
hold on
L1(1) = plot(nan, nan,'o','Color',[0, 0.4470, 0.7410],'MarkerSize',4,'LineWidth',4);           
L1(2) = plot(nan, nan,'o','Color',[0.4660, 0.6740, 0.1880],'MarkerSize',4,'LineWidth',4);           
L1(3) = plot(nan, nan,'o','Color',[0.9290, 0.6940, 0.1250],'MarkerSize',4,'LineWidth',4);
L1(4) = plot(nan, nan,'o','Color',[0.8500, 0.3250, 0.0980],'MarkerSize',4,'LineWidth',4);
leg1 = legend (L1,'6000€','9000€','12000€','15000€');
title(legend,'Car price')
leg.Title.Visible = 'on';
hold on
annotation('textbox', [0.2, 0.7, 0.1, 0.1],'String', " - - Nominal saving (% of car price)")


% Save xlsm file
filename = 'montecarlo.xlsm';
writecell(montecarlo,filename,'Sheet',1,'Range','A2');

% Table + graph
h={'Car price' ' Nom. saving (%) ' ' Nom. saving [€] ' ' Saving 75% [€] ' ' Saving 80% [€] ' ' Saving 85% [€] ' ' Saving 90% [€] '};
fig(2) = figure(2);
set(fig(2),'Position',[950 300 800 620])
T = uitable('data',table,'columnname',h,'Position',[70 50 652 97]);

axes('position',[.1,.4,.8,.5])
for i = 1:length(prices)
    plot([75 80 85 90],table(i,4:end),'-o','Color', col(i,:),'LineWidth',2)
    hold on
end
plot([75 90],[0 0],'k-','LineWidth',0.2);
legend ('6000€','9000€','12000€','15000€');
title(legend,'Car price')
leg.Title.Visible = 'on';
hold on
ylabel('Saving [€]','fontsize',12);
xlabel('Confidence','fontsize',12); xtickformat('percentage'); 

